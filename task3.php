<?php

function rewriteJsonFile(string $pathToJsonFile, string $key, $value): void {
    // JSON файл лежит по пути $pathToJsonFile. Необходимо получить содержимое этого файла,
    // добавить в него поле $key со значением $value и перезаписать.


    $orig = file_get_contents($pathToJsonFile);

   
    $arr = json_decode($orig, true);

    
    $arr[$key] = $value;

    
    file_put_contents($pathToJsonFile, json_encode($arr));
}